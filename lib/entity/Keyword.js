export class Keyword {
  constructor(name = '', description = undefined) {
    this.name = name;
    this.description = description;
  }
}
